#!/usr/bin/env python

import copy
import tf
import math
import sys
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import Pose
import geometry_msgs.msg
from moveit_msgs.msg import MotionPlanRequest

try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi


def initial_joint_state(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 4
    joint_goal[2] = 0
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] = tau / 2 
    

    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def initial_joint_state_2(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] =  tau / 8
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  tau / 4

    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_1(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = -tau / 8
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  2*tau / 4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_2(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] =  -3*tau / 8
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  3 * tau / 4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_3(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = -3*tau / 8
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  3*tau / 4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_4(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -3*tau / 8
    joint_goal[2] = 3*tau / 8
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  tau / 4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_5(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -3*tau / 8
    joint_goal[2] = 3*tau / 8
    joint_goal[3] = -2*tau / 4
    joint_goal[4] = 0
    joint_goal[5] =  tau / 2
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_6(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -2*tau / 8
    joint_goal[2] = 0
    joint_goal[3] = -tau / 4
    joint_goal[4] =  tau / 2
    joint_goal[5] =  5*tau / 8
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_7(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = -3*tau / 8
    joint_goal[3] = 0
    joint_goal[4] =  tau
    joint_goal[5] =  tau/2

    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_8(group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = -3*tau / 8
    joint_goal[3] = -tau/4
    joint_goal[4] =  tau
    joint_goal[5] =  3*tau/4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()

def joint_state_alpha(group):  # starting position before phase 2 and 3, adjust to avoid singularity
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 10
    joint_goal[2] =  -3*tau / 10
    joint_goal[3] = - tau/10 #-tau /4 - tau/10
    joint_goal[4] = 0
    joint_goal[5] =  2 * tau / 4 # 3*tau/4
    
    group.set_joint_value_target(joint_goal)
    group.go(wait=True)
    group.stop()


def move_ur5e_to_pose(pose_goal, group):
    # moveit_commander.roscpp_initialize(sys.argv)
    # rospy.init_node('ur5e_writer', anonymous=True)
    # robot = moveit_commander.RobotCommander()
    # scene = moveit_commander.PlanningSceneInterface()
    # group = moveit_commander.MoveGroupCommander("manipulator")
    group.set_pose_target(pose_goal)
    plan = group.go(wait=True)
    rospy.sleep(1)


def plan_cartesian_path(group):
    s = 0.15
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    waypoints = []

    wpose = group.get_current_pose().pose

    wpose.position.x += -s
    wpose.position.z += 0
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += s
    wpose.position.z += s
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += -s
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += -s
    wpose.position.z += s
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += s
    wpose.position.z += 0
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += -s
    wpose.position.z += -s
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += s
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += s
    wpose.position.z += -s
    waypoints.append(copy.deepcopy(wpose))

    (plan, fraction) = group.compute_cartesian_path(waypoints, 0.075, 0.0)

    return plan, fraction

def plan_cartesian_path_2(group):
    s = 0.15
    r = 15

    # Convert degrees to radians
    def degrees_to_radians(degrees):
        return (degrees * (math.pi / 180.0))
    # Convert Euler angles to quaternion
    def euler_to_quaternion(roll, pitch, yaw):
        quaternion = tf.transformations.quaternion_from_euler(roll, pitch, yaw)
        return geometry_msgs.msg.Quaternion(*quaternion)
    
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    waypoints = []

    wpose = group.get_current_pose().pose
    # Use Euler angles for orientation
    roll, pitch, yaw = tf.transformations.euler_from_quaternion(
        [wpose.orientation.x, wpose.orientation.y, wpose.orientation.z, wpose.orientation.w])


    wpose.position.x += -s
    wpose.position.z += 0
    #yaw += degrees_to_radians(r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += s
    #yaw += degrees_to_radians(-2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += -s
    wpose.position.z += 0
    #yaw += degrees_to_radians(2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += -s
    #yaw += degrees_to_radians(-2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += s
    wpose.position.z += 0
    #yaw += degrees_to_radians(2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += s
    #yaw += degrees_to_radians(-2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += s
    wpose.position.z += 0
    #yaw += degrees_to_radians(2*r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))
    wpose.position.x += 0
    wpose.position.z += -s
    #yaw += degrees_to_radians(-r)
    #wpose.orientation = euler_to_quaternion(roll, pitch, yaw)
    waypoints.append(copy.deepcopy(wpose))

    (plan, fraction) = group.compute_cartesian_path(waypoints, 0.075, 0.0)

    return plan, fraction

def cmove(xpos, zpos, group): #plans and executes a path for a simple x and z movement
    # moveit_commander.roscpp_initialize(sys.argv)
    # robot = moveit_commander.RobotCommander()
    # rospy.init_node('ur5e_writer', anonymous=True)
    # group = moveit_commander.MoveGroupCommander("manipulator")

    waypoints = []
    wpose = group.get_current_pose().pose
    wpose.position.x += xpos
    wpose.position.z += zpos
    waypoints.append(copy.deepcopy(wpose))
    (plan, fraction) = group.compute_cartesian_path(waypoints, 0.04, 0.0)

    execute_plan(plan)
    
def execute_plan(plan, group):

        # group = moveit_commander.MoveGroupCommander("manipulator")

        group.execute(plan, wait=True)

def timer(tstart, twait):
    #I built a wait function that will proceed once your program reaches [twait] seconds from the start
    tnow = rospy.get_rostime()
    tdiff = tnow.secs - tstart.secs
    while(tdiff <= twait):
        rospy.sleep(0.1)
        tnow = rospy.get_rostime()
        tdiff = tnow.secs - tstart.secs

if __name__ == '__main__':
    try:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('ur5e_writer', anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group = moveit_commander.MoveGroupCommander("manipulator")

        initial_joint_state(group)
        print("Welcome!")
        print("You will have 60 seconds to shoot the ball into moving hoop!")
        print("Watch out! The hoop will move more elusively over time!")
        input("============ Press `Enter` to start")
        #Keep track of our starting time so we can time our other motions
        # tstart = rospy.get_rostime()
        # # level_1 motion
        # # move to first pose
        joint_state_1(group) #upright
        
        # #5s
        # timer(tstart, 5)
        initial_joint_state_2(group) #side
        
        # # 10s
        # timer(tstart, 10)
        joint_state_1(group) #upright
        
        # # 15s
        # timer(tstart, 15)
        joint_state_2(group) #side
        
        # # 20s
        # # proceed to level_2 motion
        # # this motion had to be shortened as it went over the 20 second goal
        # timer(tstart, 20)
        joint_state_alpha(group)
        s = 0.15
        plan, fraction = plan_cartesian_path_2(group)
        execute_plan(plan, group)
        #8 motions, approx 2.5s each
        
        #back at starting position for level 2
        #adjust starting position to less problematic region?
        #begin level 3 at 40s
        # timer(tstart, 40)
        joint_state_alpha(group)
        s = 0.15
        #r = 15
        plan, fraction = plan_cartesian_path(group)

        execute_plan(plan, group)
        # level_3 motion

        # joint_state_4(group)
        # joint_state_5(group)
        # joint_state_6(group)
        # joint_state_7(group)
        # joint_state_8(group)
        print("Game Over!")
        print("Thank you for playing!")
        print("Check out the LCD screen for your final score!")
        input("============ Press `Enter` to return to upright position")
        initial_joint_state(group)
        
        # LV1: center of 1D motion
        # lv1_pose1 = Pose()
        # lv1_pose1.position.x = 0.3
        # lv1_pose1.position.y = 0.3
        # lv1_pose1.position.z = 0.5
        # lv1_pose1.orientation.x = 0.0
        # lv1_pose1.orientation.y = 0.0
        # lv1_pose1.orientation.z = 0.0
        # lv1_pose1.orientation.w = 1.0
       

    except rospy.ROSInterruptException:
        pass
