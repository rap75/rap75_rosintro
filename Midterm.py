#!/usr/bin/env python

## THIS CODE IS ADAPTED FROM PANDA ARM TO WORK WITH THE UR5 BY RORY PFISTER

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END OF IMPORTS

class PythonUR5Control(object):
    """PythonUR5Control"""

    # The initialization code is borrowed from the example provided on Ed Discussions. 
    # I have not edited this section as I don't want to break anything
    def __init__(self):
        super(PythonUR5Control, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## END_SUB_TUTORIAL

        ## BEGIN_SUB_TUTORIAL basic_info
        ##
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_home(self):
        # Copy class variables to local variables
        move_group = self.move_group

        # We use an exact joint goal for our home position
        # This configuration aims to avoid any possible singularities during planning or motion
        # tau is a full turn
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -tau * 0.35
        joint_goal[2] = tau * 0.35
        joint_goal[3] = -tau / 2
        joint_goal[4] = 0
        joint_goal[5] = tau / 2
        
        # Execute movement
        move_group.go(joint_goal, wait=True)

        # Prevent residual movement
        move_group.stop()

        # Print current position
        current_joints = move_group.get_current_joint_values()
        print("Movement Complete. Current Joint Values: ")
        print(current_joints)


    def go_to_zero(self):
        # Copy class variables to local variables
        move_group = self.move_group

        # We use an exact joint goal for our zero position
        # This configuration is used to get a clearer picture for our DH table and transformation matrices
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = 0
        joint_goal[2] = 0
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = 0
        
        # Execute movement
        move_group.go(joint_goal, wait=True)

        # Prevent residual movement
        move_group.stop()

        # Print current position
        current_joints = move_group.get_current_joint_values()
        print("Current Joint Values: ")
        print(current_joints)

    def plan_cartesian_path(self, letter, scale=1):

        # Copy class variables to local variables
        move_group = self.move_group

        # Create empty array to store waypoints
        waypoints = []

        # Depending on when character is inputted to this function, a different plan will be created
        if(letter == 'R'):
            wpose = move_group.get_current_pose().pose
            wpose.position.z += scale * 0.4  # First move up (z) to form left side of R
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.15  # Second Right in (x) to form top of R
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.05  # Third move diagonally to form curved part of R
            wpose.position.z -= scale * 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x -= scale * 0.05  # Forth perform Second Diagonal
            wpose.position.z -= scale * 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x -= scale * 0.15  # fifth  move left in (x) to complete loop
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.2  # Finally move diagonally to create the "leg"
            wpose.position.z -= scale * 0.2
            waypoints.append(copy.deepcopy(wpose))

        elif(letter == 'A'):
            wpose = move_group.get_current_pose().pose
            wpose.position.z += scale * 0.4  # First move up (z) and right (x) to form left side of A
            wpose.position.x += scale * 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= scale * 0.2  # Move halfway down right side of A
            wpose.position.x += scale * 0.05
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x -= scale * 0.1  # Create crossbar, then return to position
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.1  
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= scale * 0.2  # finish movement down right side of A
            wpose.position.x += scale * 0.05
            waypoints.append(copy.deepcopy(wpose))

        elif(letter == 'P'):
            wpose = move_group.get_current_pose().pose
            wpose.position.z += scale * 0.4  # First move up (z) to form left side of P
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.15  # Second Right in (x) to form top of P
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x += scale * 0.05  # Third move diagonally to form curved part of P
            wpose.position.z -= scale * 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x -= scale * 0.05  # Forth perform Second Diagonal
            wpose.position.z -= scale * 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.x -= scale * 0.15  # fifth  move left in (x) to complete loop
            waypoints.append(copy.deepcopy(wpose))

            # when you think about it, a P is really just an R missing a leg

        else: # the letter called for by the function is one that does not appear on our list
            print("ERROR: The instructions for this letter have not been defined!")
        
        # Interpolate path at 2cm increments, coarser = faster
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.02, 0.0
        ) 

        # Return planned motion, which will be executed by a different function
        return plan, fraction
    
    def execute_plan(self, plan): #EXECUTES THE MOVEMENT PATH CREATED USING plan_cartesian_path
 
        move_group = self.move_group

        move_group.execute(plan, wait=True)
        
        current_joints = move_group.get_current_joint_values()
        print("Movement Complete. Current Joint Values: ")
        print(current_joints)

def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Welcome to Rory's UR5 Controller!")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit")
        print("")
        
        myUR5 = PythonUR5Control() # Create object to control robot
        
        input("============ Setup Complete. Press `Enter` to move to home position ...")

        myUR5.go_to_home()         # Move to home position
        
        input("============ Setup Complete. Press `Enter` to plan and execute the letter 'R' ...") # Draw R
        cartesian_plan, fraction = myUR5.plan_cartesian_path('R')
        myUR5.execute_plan(cartesian_plan)

        input("============ Press `Enter` to return to home position ...")
        myUR5.go_to_home()

        input("============ Press `Enter` to plan and execute the letter 'A' ...") #Draw A
        cartesian_plan, fraction = myUR5.plan_cartesian_path('A')
        myUR5.execute_plan(cartesian_plan)
        
        input("============ Press `Enter` to return to home position ...")
        myUR5.go_to_home()

        input("============ Press `Enter` to plan and execute the letter 'P' ...") #Draw P
        cartesian_plan, fraction = myUR5.plan_cartesian_path('P')
        myUR5.execute_plan(cartesian_plan)

        input("============ Letter drawing demonstration complete! press enter to return to zero position")
        myUR5.go_to_zero()         # Go to zero position on startup

    # in case of ROS or keyboard interrupts
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()
