#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
def display(data):
    print(String)
    print('') #empty line between prints
def screen():
    rospy.init_node('screen')
    rospy.Subscriber('screen_control', String, display)
    rospy.spin()
if __name__ == '__main__':
    screen()