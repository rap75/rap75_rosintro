#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
def action(data):
    rospy.loginfo("Performing Action: %s", data.data)
def flipper():
    rospy.init_node('flipper')
    rospy.Subscriber('flipper_control', String, action)
    rospy.spin()
if __name__ == '__main__':
    flipper()