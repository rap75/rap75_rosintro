#!/usr/bin/bash
rosservice call /turtle1/teleport_absolute 1.0 1.0 1.57
rosservice call /clear
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.57]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[3.14, 0.0, 0.0]' '[0.0, 0.0, -3.14]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.36]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.82, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


