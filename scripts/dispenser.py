#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
def action(data):
    if data: #if dispense is true
        rospy.loginfo("Performing Action: Dispense Batter")
    else:
        rospy.loginfo("Stop Action: Dispense Batter")
def dispenser():
    rospy.init_node('dispenser')
    rospy.Subscriber('dispenser_control', Bool, action)
    rospy.spin()
if __name__ == '__main__':
    dispenser()
