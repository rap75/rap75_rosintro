#!/usr/bin/bash
rosservice call /turtle1/teleport_absolute 1.0 1.0 1.57
rosservice call /clear
rosservice call /turtle1/set_pen 0 255 0 5 off

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.57]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[3.14, 0.0, 0.0]' '[0.0, 0.0, -3.14]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 2.36]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.82, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 4.0 2.0 1.57 turtle2
rosservice call /turtle2/set_pen 255 0 0 5 off

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	        -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, -6.28]'
