#!/usr/bin/env python3
# Python client library for ROS
import rospy
import time
# provides the String message type
from std_msgs.msg import String

# This function contains the code for initializing the node, creating a publisher, and sending messages in a lo
def master():
    # initialize node 'master'
    rospy.init_node('master')
    # a boolean to turn on/off dispenser
    disp = rospy.Publisher('dispenser_control', bool, queue_size=10)
    # instructs flipper to either flip or remove pancake
    flip = rospy.Publisher('flipper_control', String, queue_size=10)
    # text to display on screen
    screen = rospy.Publisher('screen_output', String, queue_size=10)
    # publishing rate
    # rate = rospy.Rate(1)  # 1 Hz
    # This loop will continue until the node is shut down. It creates a message, logs it,
    # publishes the message using the pub.publish() method,
    # and then sleeps for the appropriate time to achieve the desired publishing rate.
    pancakecounter = 0
    while not rospy.is_shutdown():
        #dispense batter and update screen
        message = "Dispensing Batter..."
        rospy.loginfo("Displaying to screen: %s", message)
        screen.publish(message)

        rospy.loginfo("Sending Command to Dispense Batter")
        disp.publish(True)

        time.sleep(5) #wait 5s to stop dispensing

        rospy.loginfo("Sending Command to Stop Dispenser")
        disp.publish(False)

        message = "Cooking..."
        rospy.loginfo("Displaying to screen: %s", message)
        screen.publish(message)

        time.sleep(10) #wait 10s to cook one side of pancake
        flip.publish("flip") #send command to flip pancake

        time.sleep(8) #wait 8s to finish pancake
        flip.publish("remove") #send command to remove pancake from griddle

        pancakecounter = pancakecounter + 1

        #notify user how many pancakes are ready

        message = "Cooking Complete. More Pancakes are on the way! You now have " + str(pancakecounter) + " Pancakes!"
        rospy.loginfo("Displaying to screen: %s", message)
        screen.publish(message)

        time.sleep(2) #wait 2 seconds before beginning next pancake
if __name__ == '__main__':
    try:
        master_node()
    except rospy.ROSInterruptException:
        pass
